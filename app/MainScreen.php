<?php

namespace App;

use Gui\Application;
use Gui\Components\Button;

class MainScreen
{
    private $app, $button;

    public function run()
    {
        $application = new Application();

        $application->on('start', function () use ($application) {
            $application->getWindow()->setWindowState('fullscreen');

            $button = (new Button())
                ->setLeft(40)
                ->setTop(100)
                ->setWidth(200)
                ->setValue('Look, I\'m a button!');

            $button->on('click', function () use ($button) {
                $button->setValue('Look, I\'m a clicked button!');
            });
        });

        $application->run();
    }
}